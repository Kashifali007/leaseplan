package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class SearchMangoDefinitions {
    @Steps
    SearchMango searchMango;


    @Given("^user send a request to endpoint$")
    public void user_send_a_request_to_endpoint() {
        searchMango.searchMango();
    }

    @Then("The mango endpoint should return status {int}")
    public void the_mango_endpoint_should_return_status(int statuscode) {
        searchMango.getStatusCodeMango(statuscode);

    }

    @Then("user display mango endpoint response")
    public void user_display_mango_endpoint_response() {
        searchMango.validateResponseMango();
    }
}
