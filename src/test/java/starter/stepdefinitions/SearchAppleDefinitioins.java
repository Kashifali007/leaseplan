package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;


public class SearchAppleDefinitioins {

    @Steps
    SearchApple searchApple;

    @Given("I send a request to endpoint")
    public void i_send_a_request_to_endpoint() {
        searchApple.searchApple();
    }

    @Then("The api should return status {int}")
    public void the_api_should_return_status(int statuscode) {
        searchApple.getStatusCode(statuscode);
    }

    @Then("I display the response")
    public void i_display_the_response() {
        searchApple.validateResponse();
    }
}
