package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class SearchTofuDefinitions {

    @Steps
    SearchTofu searchTofu;

    @Given("user send a request to tofu endpoint")
    public void user_send_a_request_to_tofu_endpoint() {
        searchTofu.searchTofu();

    }

    @Then("The tofu endpoint should return status {int}")
    public void the_tofu_endpoint_should_return_status(int statuscode) {
        searchTofu.getStatusCodeTofu(statuscode);
    }

    @Then("user validate the result displayed for tofu")
    public void user_validate_the_result_displayed_for_tofu() {
        searchTofu.validateResponseTofu();
    }
}
