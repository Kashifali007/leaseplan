package starter.stepdefinitions;

import com.fasterxml.jackson.core.type.TypeReference;
import com.pojos.PojoTofu;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import starter.com.endpoints.EndPoints;

import java.lang.reflect.Type;
import java.util.List;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

public class SearchTofu {

    Logger log = LogManager.getLogger(SearchAppleDefinitioins.class);
    public Response response;
    private EnvironmentVariables environmentVariables;
    String baseURI;
    List<PojoTofu> pojoTofus;


    public String getBaseUri() {
        log.info("executing getBaseURI method");
        baseURI = environmentVariables.optionalProperty("restapi.baseurl").orElse("https://waarkoop-server.herokuapp.com/api/v1/search/test");
        log.info("baseURI [" + baseURI + "]");
        return baseURI;
    }


    @Step
    public void searchTofu() {
        try {
            log.info("executing send request to apple endpoint [" + EndPoints.GET_APPLE + "]");
            response = (Response) SerenityRest
                    .given()
                    .contentType(ContentType.JSON)
                    .header("Content-type", "application/json")
                    .when()
                    .get(getBaseUri() + EndPoints.GET_TOFU);

            //deserializing the json object and mapping the response with pojo
            Type type = new TypeReference<List<PojoTofu>>() {
            }.getType();
            pojoTofus = response.as(type);
            log.info(response.body().print());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Step
    public void getStatusCodeTofu(int statuscode) {
        log.info("validating searchApple status code");
        log.info("Status code [" + statuscode + "]");
        restAssuredThat(response -> response.statusCode(statuscode));
    }

    @Step
    public void validateResponseTofu() {
        log.info("displaying the response");
        //failing intentionally here
        restAssuredThat(response -> response.body("price", notNullValue()));
    }
}
