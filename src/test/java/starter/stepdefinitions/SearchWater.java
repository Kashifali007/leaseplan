package starter.stepdefinitions;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import starter.com.endpoints.EndPoints;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.notNullValue;

public class SearchWater {
    Logger log = LogManager.getLogger(SearchAppleDefinitioins.class);
    public Response response;
    private EnvironmentVariables environmentVariables;
    String baseURI;


    public String getBaseUri() {
        log.info("executing getBaseURI method");
        baseURI = environmentVariables.optionalProperty("restapi.baseurl").orElse("https://waarkoop-server.herokuapp.com/api/v1/search/test");
        log.info("baseURI [" + baseURI + "]");
        return baseURI;
    }


    @Step
    public void searchWater() {
        try {
            log.info("executing send request to apple endpoint [" + EndPoints.GET_APPLE + "]");
            response = (Response) SerenityRest
                    .given()
                    .contentType(ContentType.JSON)
                    .header("Content-type", "application/json")
                    .when()
                    .get(getBaseUri() + EndPoints.GET_WATER);
            log.info(response.body().print());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Step
    public void getStatusCodeWater(int statuscode) {
        log.info("validating searchApple status code");
        log.info("Status code [" + statuscode + "]");
        restAssuredThat(response -> response.statusCode(statuscode));
    }

    @Step
    public void validateResponseWater() {
        log.info("displaying the response");
        restAssuredThat(response -> response.body("price", notNullValue()));
        restAssuredThat(response -> response.body("unit", notNullValue()));
        restAssuredThat(response -> response.body("isPromo", notNullValue()));
        // response.then().assertThat().body(matchesJsonSchemaInClasspath("schema/water.json"));
    }
}
