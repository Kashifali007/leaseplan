package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class SearchBananaDefinitions {
    @Steps
    SearchBanana searchBanana;

    @Given("user send a request to banana endpoint")
    public void user_send_a_request_to_banana_endpoint() {
        searchBanana.searchBanana();
    }

    @Then("The banana endpoint should return status {int}")
    public void the_banana_endpoint_should_return_status(int statuscode) {
        searchBanana.getStatusCode(statuscode);
    }
}
