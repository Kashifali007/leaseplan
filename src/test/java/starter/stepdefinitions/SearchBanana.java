package starter.stepdefinitions;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import starter.com.endpoints.EndPoints;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class SearchBanana {
    Logger log = LogManager.getLogger(SearchAppleDefinitioins.class);
    public Response response;
    private EnvironmentVariables environmentVariables;
    String baseURI;


    public String getBaseUri() {
        log.info("executing getBaseURI method");
        baseURI = environmentVariables.optionalProperty("restapi.baseurl").orElse("https://waarkoop-server.herokuapp.com/api/v1/search/test");
        log.info("baseURI [" + baseURI + "]");
        return baseURI;
    }


    @Step
    public void searchBanana() {
        try {
            log.info("executing send request to apple endpoint [" + EndPoints.GET_APPLE + "]");
            response = (Response) SerenityRest
                    .given()
                    .contentType(ContentType.JSON)
                    .header("Content-type", "application/json")
                    .when()
                    .get(getBaseUri() + EndPoints.GET_BANANA);

            log.info(response.body().print());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Step
    public void getStatusCode(int statuscode) {
        log.info("validating searchApple status code");
        log.info("Status code [" + statuscode + "]");
        restAssuredThat(response -> response.statusCode(statuscode));
    }

}
