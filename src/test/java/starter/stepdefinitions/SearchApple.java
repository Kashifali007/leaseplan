package starter.stepdefinitions;

import com.fasterxml.jackson.core.type.TypeReference;
import com.pojos.PojoApple;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import starter.com.endpoints.EndPoints;

import java.lang.reflect.Type;
import java.util.List;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.notNullValue;

public class SearchApple {
    Logger log = LogManager.getLogger(SearchAppleDefinitioins.class);
    public Response response;
    private EnvironmentVariables environmentVariables;
    String baseURI;
    PojoApple pojoApple;
    List<PojoApple> appleList;


    public String getBaseUri() {
        log.info("executing getBaseURI method");
        baseURI = environmentVariables.optionalProperty("restapi.baseurl").orElse("https://waarkoop-server.herokuapp.com/api/v1/search/test");
        log.info("baseURI [" + baseURI + "]");
        return baseURI;
    }


    @Step
    public void searchApple() {
        try {
            log.info("executing send request to apple endpoint [" + EndPoints.GET_APPLE + "]");
            response = (Response) SerenityRest
                    .given()
                    .contentType(ContentType.JSON)
                    .header("Content-type", "application/json")
                    .when()
                    .get(getBaseUri() + EndPoints.GET_APPLE);

            //deserializing the json object and mapping the response with pojo
            Type type = new TypeReference<List<PojoApple>>() {
            }.getType();
            appleList = response.as(type);
            log.info(response.body().print());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Step
    public void getStatusCode(int statuscode) {
        log.info("validating searchApple status code");
        log.info("Status code [" + statuscode + "]");
        restAssuredThat(response -> response.statusCode(statuscode));
    }

    @Step
    public void validateResponse() {
        log.info("displaying the response");
        restAssuredThat(response -> response.body("provider", notNullValue()));
    }

    @Step
    public void schemaValidation() {
        response.then().assertThat().body(matchesJsonSchemaInClasspath("schema/apple.json"));
    }
}

