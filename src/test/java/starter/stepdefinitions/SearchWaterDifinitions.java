package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class SearchWaterDifinitions {

    @Steps
    SearchWater searchWater;

    @Given("user send a request to water endpoint")
    public void user_send_a_request_to_water_endpoint() {
        searchWater.searchWater();

    }

    @Then("The water endpoint should return status {int}")
    public void the_water_endpoint_should_return_status(int statusCode) {
        searchWater.getStatusCodeWater(statusCode);
    }

    @Then("user validate the result displayed for water")
    public void user_validate_the_result_displayed_for_water() {
        searchWater.validateResponseWater();
    }

}
