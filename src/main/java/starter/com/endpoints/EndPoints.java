package starter.com.endpoints;

public class EndPoints {
    // service endpoints is defined here
    public static final String GET_APPLE = "/apple";
    public static final String GET_MANGO = "/mango";
    public static final String GET_TOFU = "/tofu";
    public static final String GET_WATER = "/water";
    public static final String GET_CAR = "/car";
    public static final String GET_BANANA = "/banana";
}
